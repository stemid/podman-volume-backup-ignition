data "ignition_file" "backup_script" {
  path = "${var.settings.script_base}/podman-volume-backup.bash"
  content {
    content = file("${path.module}/templates/podman-volume-backup.bash")
  }
  uid = var.settings.uid
  gid = var.settings.gid
 
  # 0750
  mode = 488
}

data "ignition_file" "restore_script" {
  path = "${var.settings.script_base}/podman-volume-restore.bash"
  content {
    content = file("${path.module}/templates/podman-volume-restore.bash")
  }
  uid = var.settings.uid
  gid = var.settings.gid
 
  # 0750
  mode = 488
}

data "ignition_directory" "config_base" {
  path = "${var.settings.config_base}/podman-volume-backup"
  mode = 488
  uid = var.settings.uid
  gid = var.settings.gid
}

data "ignition_file" "backup_environment" {
  path = "${var.settings.config_base}/podman-volume-backup/environment"
  content {
    content = templatefile("${path.module}/templates/environment", {
      s3 = var.s3
    })
  }
  mode = 384
  uid = var.settings.uid
  gid = var.settings.gid
}

data "ignition_file" "backup_unit" {
  path = "${var.settings.systemd_base}/podman-volume-backup@.service"
  content {
    content = templatefile("${path.module}/templates/podman-volume-backup@.service", {
      settings = var.settings
    })
  }
  uid = var.settings.uid
  gid = var.settings.gid
}

data "ignition_file" "restore_unit" {
  path = "${var.settings.systemd_base}/podman-volume-restore@.service"
  content {
    content = templatefile(
      "${path.module}/templates/podman-volume-restore@.service",
      {
        settings = var.settings
      }
    )
  }
  uid = var.settings.uid
  gid = var.settings.gid
}

data "ignition_file" "backup_timer" {
  path = "${var.settings.systemd_base}/podman-volume-backup@.timer"
  content {
    content = file("${path.module}/templates/podman-volume-backup@.timer")
  }
  uid = var.settings.uid
  gid = var.settings.gid
}

data "ignition_config" "config" {
  directories = [
    data.ignition_directory.config_base.rendered,
  ]

  files = [
    data.ignition_file.backup_script.rendered,
    data.ignition_file.restore_script.rendered,
    data.ignition_file.backup_environment.rendered,
    data.ignition_file.backup_unit.rendered,
    data.ignition_file.restore_unit.rendered,
    data.ignition_file.backup_timer.rendered
  ]
}
