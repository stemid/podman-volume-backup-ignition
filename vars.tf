variable "settings" {
  type = object({
    systemd_base = string
    script_base = string
    config_base = string
    uid = number
    gid = number
  })
  default = {
    systemd_base = "/etc/systemd/system"
    script_base = "/usr/local/bin"
    config_base = "/etc"
    uid = 0
    gid = 0
  }
}

variable "s3" {
  type = object({
    aws_access_key_id = string
    aws_secret_access_key = string
    aws_s3_bucket = string
    aws_default_region = string
    endpoint_url = optional(string, "")
  })
}
