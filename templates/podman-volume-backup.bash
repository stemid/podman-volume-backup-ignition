#!/usr/bin/env bash

set -xeu

shopt -s nullglob

function aws() {
  podman run -i --rm --name awscli -e AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY -e AWS_S3_BUCKET -e AWS_DEFAULT_REGION \
    docker.io/amazon/aws-cli "$@"
}

function jq() {
  podman run -i --rm --name jq ghcr.io/jqlang/jq:1.7 "$@"
}

volName=$1

test -n "$volName" || exit 1
podman volume exists "$volName" || exit 1

numBackups=$(aws s3api ${S3_ENDPOINT_URL:+"--endpoint-url=$S3_ENDPOINT_URL"} \
  list-objects-v2 \
  --bucket="$AWS_S3_BUCKET" \
  --query "Contents[?Key=='podman/$volName.tar']" | jq '. | length')

if [[ $numBackups -gt 0 ]]; then
  sizeBackup=$(aws s3api ${S3_ENDPOINT_URL:+"--endpoint-url=$S3_ENDPOINT_URL"} \
    list-objects-v2 \
    --bucket="$AWS_S3_BUCKET" \
    --query "Contents[?Key=='podman/$volName.tar']" | jq -r '.[0].Size')

  if [[ $sizeBackup -gt 0 ]]; then
    aws ${S3_ENDPOINT_URL:+"--endpoint-url=$S3_ENDPOINT_URL"} s3 \
      mv "s3://$AWS_S3_BUCKET/podman/${volName}.tar" "s3://$AWS_S3_BUCKET/podman/${volName}-$(date -In).tar"
  fi
fi

podman volume export "$volName" | aws ${S3_ENDPOINT_URL:+"--endpoint-url=$S3_ENDPOINT_URL"} \
  s3 cp - "s3://$AWS_S3_BUCKET/podman/${volName}.tar"
