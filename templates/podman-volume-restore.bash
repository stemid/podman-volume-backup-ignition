#!/usr/bin/env bash

set -xeu
shopt -s nullglob

function aws() {
  podman run -i --rm --name awscli \
    -e AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY \
    -e AWS_S3_BUCKET \
    -e AWS_DEFAULT_REGION \
    docker.io/amazon/aws-cli "$@"
}

function jq() {
  podman run -i --rm ghcr.io/jqlang/jq:1.7 "$@"
}

volName=$1
test -n "$volName" || exit 1

podman volume exists "$volName" || podman volume create "$volName"

files=($(podman volume inspect --format='{{ .Mountpoint }}' "$volName")/*)
# Do not import volume if files exist in it.
test ${#files[@]} -gt 0 && exit 0

# Verify that backup file exists
numBackups=$(aws s3api ${S3_ENDPOINT_URL:+"--endpoint-url=$S3_ENDPOINT_URL"} \
  list-objects-v2 \
  --bucket="$AWS_S3_BUCKET" \
  --query "Contents[?Key=='podman/$volName.tar']" | jq '. | length')

# No backups to restore, just exit nicely so next service can start.
test $numBackups -eq 0 && exit 0

# Verify it's not too small to be useful
sizeBackup=$(aws s3api ${S3_ENDPOINT_URL:+"--endpoint-url=$S3_ENDPOINT_URL"} \
  list-objects-v2 \
  --bucket="$AWS_S3_BUCKET" \
  --query "Contents[?Key=='podman/$volName.tar']" | jq -r '.[0].Size')
test $sizeBackup -gt 4096 || exit 0

# Only create volume if we have something to restore.
podman volume exists "$volName" || podman volume create "$volName"
aws ${S3_ENDPOINT_URL:+"--endpoint-url=$S3_ENDPOINT_URL"} \
  s3 cp "s3://$AWS_S3_BUCKET/podman/${volName}.tar" - | podman volume import "$volName" -
